# Ranking Sustentável - Front-End

Repository created using Vue CLI 3, using plugins:

- `@vue/cli-service`;
- `@ky-is/vue-cli-plugin-tailwind`;
- `@vue/cli-plugin-babel`;
- `@vue/cli-plugin-e2e-cypress` (plugin not present in `package.json`, but files were kept);
- `@vue/cli-plugin-pwa`;
- `@vue/cli-plugin-typescript`;
- `@vue/cli-plugin-unit-jest`;
- `vue-cli-plugin-apollo`.

## How to run

```bash
# run docker-compose
docker-compose build
docker-compose up

# access via http://localhost:8080
```

## NPM Script Commands

### Installs dependencies from package.json

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Run your end-to-end tests

```
npm run test:e2e
```

### Run your unit tests

```
npm run test:unit
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
